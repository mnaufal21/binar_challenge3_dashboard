const { Router } = require('express')
const router = Router()
const satpam = require('./../middleware/authMiddleware')

router.get('/', satpam, (req, res) => {
    res.render('cars')
})

router.post('/login', (req, res) => {
    res.app.locals.token = "token123"
    res.redirect('/')
})

module.exports = router