const express = require('express')
const app = express()
const routes = require('./routes/routes')
const path = require("path")
const port = process.env.PORT || 8000

app.set('view engine', 'ejs')

app.use(express.static(path.join(__dirname, 'public')))
app.use('/', routes)

app.listen(port, ()=>{
    console.log('Server running on port 8000')
})